public class ClassB extends ClassA{
	private int b;
	
	public ClassB() {
		
	}
	
	
	public String toString() {
		return super.toString()+" "+b;
	}
	
	public int calc(int x) {
		return b+x;
	}
}
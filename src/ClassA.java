public class ClassA{
	private int a = 8;
	
	public ClassA() {
		a = 5;
	}
	
	public String toString() {
		return " "+a;
	}
	
	public int calc(int x) {
		return x*a;
	}
}